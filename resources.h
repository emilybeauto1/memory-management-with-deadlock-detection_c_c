/* Emily Beauto
 * CS 4760
 * Project 5 Resource Management
 * 4/25/23
 * resources.h
 */

#ifndef RESOURCES_H
#define RESOURCES_H

struct resources {
	int R0; 
	int R1;
	int R2;
	int R3;
	int R4;
	int R5;
	int R6;
	int R7;
	int R8;
	int R9;
} resources;

#define MAXRESOURCES 20

#endif
