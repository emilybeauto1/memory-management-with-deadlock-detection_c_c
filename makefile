CC	= g++ -g3 -std=c++11
CFLAGS  = -g3
TARGET1 = user_proc
TARGET2 = oss 

OBJS1	= user_proc.o
OBJS2	= oss.o

all:	$(TARGET1) $(TARGET2)

$(TARGET1):	$(OBJS1)
	$(CC) -o $(TARGET1) $(OBJS1)

$(TARGET2):	$(OBJS2)
	$(CC) -o $(TARGET2) $(OBJS2)

worker.o:	user_proc.cpp
	$(CC) $(CFLAGS) -c user_proc.cpp 

oss.o:	oss.cpp
	$(CC) $(CFLAGS) -c oss.cpp

clean:
	/bin/rm -f *.o $(TARGET1) $(TARGET2)

