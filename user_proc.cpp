/* Emily Beauto
 * CS 4760
 * Project 5 Resource Management
 * 4/25/23
 * worker.cpp
 */


#include<unistd.h>
#include<sys/types.h>
#include<stdio.h>
#include<stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <errno.h>
#include <string.h>
#include <ctime>
#include "resources.h"

#define PERMS 0644
typedef struct msgbuffer {
	long mtype;
	int choice;
	int resource;
	int pid;
	int term;
	
} msgbuffer;



int main(int argc, char** argv) 
{
	//allocation; establish clock connection	
  	const int keySec = 1234; 
        const int keyNano = 1235; 
        
	int idSec = shmget( keySec , sizeof(int)* 2 , IPC_CREAT | 0666  );
        if (idSec <= 0) {
                fprintf(stderr,"Shared memory get failed\n");
                exit(1);
        }

        int idNano = shmget( keyNano , sizeof(int)* 2 , IPC_CREAT | 0666  );
        if (idNano <= 0) {
                fprintf(stderr,"Shared memory get failed\n");
                exit(1);
        }

        //clock attaching- get pointer to shared block
        int *clockSec = (int*)shmat( idSec, 0 , 0 );
        if (clockSec <= 0) {
                fprintf(stderr,"Shared memory attach failed\n");
                exit(1);
        }

         //clock attaching- get pointer to shared block
        int *clockNano = (int*)shmat( idNano, 0, 0 );
        if (clockNano <= 0) {
                fprintf(stderr,"Shared memory attach failed\n");
                exit(1);
        }




//	printf("in pid  %d   time: %d:%d\n",getpid(), *clockSec, *clockNano);

	//establish message queue
	msgbuffer buf;
	buf.mtype = 1;
	int msqid = 0;
	key_t key;

	// get a key for our message queue
	if ((key = ftok("msgq.txt", 1)) == -1) {
		perror("ftok");
		exit(1);
	}

	// create our message queue
	if ((msqid = msgget(key, PERMS)) == -1) {
		perror("msgget in child");
		exit(1);
	}


	int maxTimeBetweenNewRNS = 2500000000;
        int maxTimeBetweenNewRSecs = 0;
	int nextTermCheckSec= 0;
	int nextTermCheckNano = 0; 
	int startSec= *clockSec;
	int startNs = *clockNano;
	bool oneSecFlag = false;
	int nextLaunchSec = *clockSec, nextLaunchNano=*clockNano;		
	struct resources currentr;
	currentr.R0 = 0;
        currentr.R1 = 0;
        currentr.R2 = 0;
        currentr.R3 = 0;
        currentr.R4 = 0;
        currentr.R5 = 0;
	currentr.R6 = 0;
        currentr.R7 = 0;
        currentr.R8 = 0;
        currentr.R9 = 0;	

	srand(time(0) ^ getpid());	
	
	bool exitflag = false;
	int Rnumber = -1;	//which resource
	int r = -1;	//percentage
	while(true)
	{
		if(*clockSec > startSec)        //see if process is one second old
                {
                        oneSecFlag = true;
                }
		Rnumber = rand() % 10; //rand() % 10;		//0-9
		r = rand() % 100; 		//0-99
		if(r <= 90)	//90% requesting
		{
			buf.choice = 1;
			//check if 20 request have been made
			switch(Rnumber)
			{
				case 0:
				{
					if((MAXRESOURCES - currentr.R0) > 0)	//if not 20 currently, send request wait for reponse
					{
						buf.mtype = getppid();
				                buf.resource = Rnumber;
               					buf.pid = getpid();
               				 	//send msg to oss with R number
						if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                       					perror("R0 msgsnd to parent failed\n");
                    					exit(1);
                				}
						// receive a message, but only one for us
				                if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                        				perror("R0 failed to receive message from parent\n");
                       	 				exit(1);
                				}
						currentr.R0 += 1;
					}
					break;
				}
				case 1:
                                {
                                        if((MAXRESOURCES - currentr.R1) > 0)    //if not 20 currently, send request wait for reponse
                                        {
                                                buf.mtype = getppid();
                                                buf.resource = Rnumber;
                                                buf.pid = getpid();
                                                //send msg to oss with R number
                                                if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                                                        perror("R1 msgsnd to parent failed\n");
                                                        exit(1);
                                                }
                                                // receive a message, but only one for us
                                                if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                                                        perror("R1 failed to receive message from parent\n");
                                                        exit(1);
                                                }
                                                currentr.R1 += 1;
                                        }
					break;
                                }
				case 2:
                                {
                                        if((MAXRESOURCES - currentr.R2) > 0)    //if not 20 currently, send request wait for reponse
                                        {
                                                buf.mtype = getppid();
                                                buf.resource = Rnumber;
                                                buf.pid = getpid();
                                                //send msg to oss with R number
                                                if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                                                        perror("R2 msgsnd to parent failed\n");
                                                        exit(1);
                                                }
                                                // receive a message, but only one for us
                                                if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                                                        perror("R2 failed to receive message from parent\n");
                                                        exit(1);
                                                }
                                                currentr.R2 += 1;
                                        }
					break;
                                }
				case 3:
                                {
                                        if((MAXRESOURCES - currentr.R3) > 0)    //if not 20 currently, send request wait for reponse
                                        {
                                                buf.mtype = getppid();
                                                buf.resource = Rnumber;
                                                buf.pid = getpid();
                                                //send msg to oss with R number
                                                if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                                                        perror("R3 msg send to parent failed\n");
                                                        exit(1);
                                                }
                                                // receive a message, but only one for us
                                                if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                                                        perror("R3 failed to receive message from parent\n");
                                                        exit(1);
                                                }
                                                currentr.R3 += 1;
                                        }
					break;
                                }
				case 4:
                                {
                                        if((MAXRESOURCES - currentr.R4) > 0)    //if not 20 currently, send request wait for reponse
                                        {
                                                buf.mtype = getppid();
                                                buf.resource = Rnumber;
                                                buf.pid = getpid();
                                                //send msg to oss with R number
                                                if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                                                        perror("R4 msgsnd to parent failed\n");
                                                        exit(1);
                                                }
                                                // receive a message, but only one for us
                                                if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                                                        perror("R4 failed to receive message from parent\n");
                                                        exit(1);
                                                }
                                                currentr.R4 += 1;
                                        }
					break;
                                }
				case 5:
                                {
                                        if((MAXRESOURCES - currentr.R5) > 0)    //if not 20 currently, send request wait for reponse
                                        {
                                                buf.mtype = getppid();
                                                buf.resource = Rnumber;
                                                buf.pid = getpid();
                                                //send msg to oss with R number
                                                if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                                                        perror("R5 msgsnd to parent failed\n");
                                                        exit(1);
                                                }
                                                // receive a message, but only one for us
                                                if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                                                        perror("R5 failed to receive message from parent\n");
                                                        exit(1);
                                                }
                                                currentr.R5 += 1;
                                        }
				        break;
                                }
				case 6:
                                {
                                        if((MAXRESOURCES - currentr.R6) > 0)    //if not 20 currently, send request wait for reponse
                                        {
                                                buf.mtype = getppid();
                                                buf.resource = Rnumber;
                                                buf.pid = getpid();
                                                //send msg to oss with R number
                                                if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                                                        perror("R6 msgsnd to parent failed\n");
                                                        exit(1);
                                                }
                                                // receive a message, but only one for us
                                                if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                                                        perror("R6 failed to receive message from parent\n");
                                                        exit(1);
                                                }
                                                currentr.R6 += 1;
                                        }
				        break;       
                                }
				case 7:
                                {
                                        if((MAXRESOURCES - currentr.R7) > 0)    //if not 20 currently, send request wait for reponse
                                        {
                                                buf.mtype = getppid();
                                                buf.resource = Rnumber;
                                                buf.pid = getpid();
                                                //send msg to oss with R number
                                                if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                                                        perror("R7 msgsnd to parent failed\n");
                                                        exit(1);
                                                }
                                                // receive a message, but only one for us
                                                if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                                                        perror("R7 failed to receive message from parent\n");
                                                        exit(1);
                                                }
                                                currentr.R7 += 1;
                                        }
				        break;
                                }
				case 8:
                                {
                                        if((MAXRESOURCES - currentr.R8) > 0)    //if not 20 currently, send request wait for reponse
                                        {
                                                buf.mtype = getppid();
                                                buf.resource = Rnumber;
                                                buf.pid = getpid();
                                                //send msg to oss with R number
                                                if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                                                        perror("R8 msgsnd to parent failed\n");
                                                        exit(1);
                                                }
                                                // receive a message, but only one for us
                                                if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                                                        perror("R8 failed to receive message from parent\n");
                                                        exit(1);
                                                }
                                                currentr.R8 += 1;
                                        }
		  		       	break;
                                }
				case 9:
                                {
                                        if((MAXRESOURCES - currentr.R9) > 0)    //if not 20 currently, send request wait for reponse
                                        {
                                                buf.mtype = getppid();
                                                buf.resource = Rnumber;
                                                buf.pid = getpid();
                                                //send msg to oss with R number
                                                if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                                                        perror("R9 msgsnd to parent failed\n");
                                                        exit(1);
                                                }
                                                // receive a message, but only one for us
                                                if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                                                        perror("R9 failed to receive message from parent\n");
                                                        exit(1);
                                                }
                                                currentr.R9 += 1;
                                        }
				        break;
                                }

			}
	
		}
		else if(r >= 91 && r <=96)	//releasing it 
		{
			buf.choice = 2;
			switch(Rnumber)
			{
				case 0:
				{
					if(currentr.R0 > 0){
						buf.mtype = getppid();
                        			buf.resource = Rnumber;
                        			buf.pid = getpid();
                      				//send msg to oss with R number
                       				if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                        				perror("R0 release msgsnd to parent failed\n");
                                			exit(1);
			                        }
						// receive a message, but only one for us
                                                if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                                                        perror("R0 release failed to receive message from parent\n");
                                                        exit(1);
                                                }
                                                currentr.R0 -= 1;	
					}
					break;
				}
				case 1:
                                {
                                        if(currentr.R1 > 0){
                                                buf.mtype = getppid();
                                                buf.resource = Rnumber;
                                                buf.pid = getpid();
                                                //send msg to oss with R number
                                                if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                                                        perror("R1 release msgsnd to parent failed\n");
                                                        exit(1);
                                                }
                                                // receive a message, but only one for us
                                                if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                                                        perror("R1 release failed to receive message from parent\n");
                                                        exit(1);
                                                }
                                                currentr.R1 -= 1;
                                        }
                                        break;
                                }
				case 2:
                                {
                                        if(currentr.R2 > 0){
                                                buf.mtype = getppid();
                                                buf.resource = Rnumber;
                                                buf.pid = getpid();
                                                //send msg to oss with R number
                                                if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                                                        perror("R2 release msgsnd to parent failed\n");
                                                        exit(1);
                                                }
                                                // receive a message, but only one for us
                                                if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                                                        perror("R2 release failed to receive message from parent\n");
                                                        exit(1);
                                                }
                                                currentr.R2 -= 1;
                                        }
                                        break;
                                }
				case 3:
                                {
                                        if(currentr.R3 > 0){
                                                buf.mtype = getppid();
                                                buf.resource = Rnumber;
                                                buf.pid = getpid();
                                                //send msg to oss with R number
                                                if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                                                        perror("R3 release msgsnd to parent failed\n");
                                                        exit(1);
                                                }
                                                // receive a message, but only one for us
                                                if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                                                        perror("R3 release failed to receive message from parent\n");
                                                        exit(1);
                                                }
                                                currentr.R3 -= 1;
                                        }
                                        break;
                                }
				case 4:
                                {
                                        if(currentr.R4 > 0){
                                                buf.mtype = getppid();
                                                buf.resource = Rnumber;
                                                buf.pid = getpid();
                                                //send msg to oss with R number
                                                if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                                                        perror("R4 release msgsnd to parent failed\n");
                                                        exit(1);
                                                }
                                                // receive a message, but only one for us
                                                if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                                                        perror("R4 release failed to receive message from parent\n");
                                                        exit(1);
                                                }
                                                currentr.R4 -= 1;
                                        }
                                        break;
                                }
				case 5:
                                {
                                        if(currentr.R5 > 0){
                                                buf.mtype = getppid();
                                                buf.resource = Rnumber;
                                                buf.pid = getpid();
                                                //send msg to oss with R number
                                                if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                                                        perror("R5 release msgsnd to parent failed\n");
                                                        exit(1);
                                                }
                                                // receive a message, but only one for us
                                                if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                                                        perror("R5 release failed to receive message from parent\n");
                                                        exit(1);
                                                }
                                                currentr.R5 -= 1;
                                        }
                                        break;
                                }
				case 6:
                                {
                                        if(currentr.R6 > 0){
                                                buf.mtype = getppid();
                                                buf.resource = Rnumber;
                                                buf.pid = getpid();
                                                //send msg to oss with R number
                                                if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                                                        perror("R6 release msgsnd to parent failed\n");
                                                        exit(1);
                                                }
                                                // receive a message, but only one for us
                                                if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                                                        perror("R6 release failed to receive message from parent\n");
                                                        exit(1);
                                                }
                                                currentr.R6 -= 1;
                                        }
                                        break;
                                }
				case 7:
                                {
                                        if(currentr.R7 > 0){
                                                buf.mtype = getppid();
                                                buf.resource = Rnumber;
                                                buf.pid = getpid();
                                                //send msg to oss with R number
                                                if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                                                        perror("R7 release msgsnd to parent failed\n");
                                                        exit(1);
                                                }
                                                // receive a message, but only one for us
                                                if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                                                        perror("R7 release failed to receive message from parent\n");
                                                        exit(1);
                                                }
                                                currentr.R7 -= 1;
                                        }
                                        break;
                                }
				case 8:
                                {
                                        if(currentr.R8 > 0){
                                                buf.mtype = getppid();
                                                buf.resource = Rnumber;
                                                buf.pid = getpid();
                                                //send msg to oss with R number
                                                if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                                                        perror("R8 release msgsnd to parent failed\n");
                                                        exit(1);
                                                }
                                                // receive a message, but only one for us
                                                if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                                                        perror("R8 release failed to receive message from parent\n");
                                                        exit(1);
                                                }
                                                currentr.R8 -= 1;
                                        }
                                        break;
                                }
				case 9:
                                {
                                        if(currentr.R9 > 0){
                                                buf.mtype = getppid();
                                                buf.resource = Rnumber;
                                                buf.pid = getpid();
                                                //send msg to oss with R number
                                                if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
                                                        perror("R9 release msgsnd to parent failed\n");
                                                        exit(1);
                                                }
                                                // receive a message, but only one for us
                                                if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                                                        perror("R9 release failed to receive message from parent\n");
                                                        exit(1);
                                                }
                                                currentr.R9 -= 1;
                                        }
                                        break;
                                }

			}
			
		}
		else if(r >= 97 && r <= 99 ) //termination
		{
			buf.choice = 3;
			buf.mtype = getppid();
                        buf.pid = getpid();
			//send msg to oss with R number
                       	if (msgsnd(msqid,&buf, sizeof(msgbuffer)-sizeof(long),0) == -1) {
        	        	perror("Term msgsnd to parent failed\n");
                                exit(1);
                       	}
			if ( msgrcv(msqid, &buf, sizeof(msgbuffer), getpid(), 0) == -1) {
                       		perror("Term failed to receive message from parent\n");
                      	        exit(1);
                     	}
			exit(0);
		}	
		else
		{
			printf("error with determining percentage. Calling for Termination.\n");
			exit(1);
		}
	}
	return 0;
}

