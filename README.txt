Emily Beauto
Project 5 Resource Management


How to compile and run:
	-Type make 
	-Then execute to following command:

	
Calling:
	./oss -f logfile [-v] 
		-logfile is replaced with the name of file to open for output.
		-optional v to turn off verbose 
		    If -f is not provided automatic "logfile" will be opened
		    If -v is not included verbose is automoatically turned on 
		EX: ./oss -f file -v   	   or        ./oss -h    for help message

	
	Parameters for user_proc.cpp
	./user_proc
		where communication is sent via message queue
		EX: ./user_proc    	//executed in oss.cpp


***Please note that the internal clock changes depending on the server load. If you need to change the speed, change the increment value on line 111 in oss.cpp***
***Deadlock Detection runs every 3 secs, can change on line 2022 in oss.cpp***
*** Logfile prints to 100,000 lines as specified in the directions on Canvas ***


Communicating:
	-used message queues to communicate requests, allocation, and release of resources


Policy to Kill Deadlocked Processes:
	-Standard procedure is to kill all deadlocked processes which is returned via a deadlockedq queue.  
	-Deadlock Detection is currently run every three seconds as it was clogging up the logfile. (Mark okayed this)


Extra Headers:
	-resources.h is used to create the resources and instances. If wanted to change max instances you can do that in this file.


Logging Files:
	-git (.git subdirectory) was used to keep track of versioning files


Outstanding Problems: 
	-N/A


Problems Encounter:
	-implementing deadlock detection algorithm. Solved by asking questions and trial and error.
	-determining when R's were deadlocked. Solved by observing the process tables a each instance.
	-closing message queue. Solved with msgctl.
	-Originally setting up the clock in shared memory, with different keys. Problem was solved after looking at the tutorial and asking questions. 
	-Freeing up memory when signal handling. Solved with global pointers so I can free up memory before terminating program.
	 
