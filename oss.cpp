/* Emily Beauto
 * CS 4760
 * Project 5 Resource Management
 * 4/25/23
 * oss.cpp
 */

#include<unistd.h>
#include<sys/types.h>
#include<stdio.h>
#include<stdlib.h>
#include <sys/time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <signal.h>
#include <sys/msg.h>
#include <errno.h>
#include <ctime>
#include <sys/wait.h>
#include <string.h>
#include <queue>
#include "resources.h"
#include <string>  
#include <iostream>

using namespace std;

//stackoverflow example of converting int to char * in c
char *convert(char *destination, int i){
	sprintf(destination, "%d", i);
	return destination;
}

#define ITOCHAR(n) convert((char[41]) { 0 },(n))


//for message queue
#define PERMS 0644

typedef struct msgbuffer {
	long mtype;
	int choice;
	int resource;
	int pid;
	int term;
} msgbuffer;

//global variables- frees memory in main and signal handlings 
int keySec, keyNano, idSec, idNano, msqid;
int *clockSec,*clockNano;        


//process table structure
struct PCB {
	int occupied; 		// either true or false
	pid_t pid; 		// process id of this child
	int startSeconds; 	// time when it was forked
	int startNano; 		// time when it was forked
	int R1; 
	int R2;
	int R3;
	int R4;	
	int R5;
	int R6;
	int R7;
	int R8;
	int R9;
	int R0;
	int blocked;		//blocked or not
};

//frees up memory and kills program (after 60 second timer goes off in main)
static void alarmHandler(int sig)
{
	shmdt( clockSec );    // Detach from the shared memory segment
	shmctl( idSec, IPC_RMID, NULL ); // Free shared memory segment shm_id
	shmdt( clockNano );    // Detach from the shared memory segment
	shmctl( idNano, IPC_RMID, NULL ); // Free shared memory segment shm_id
        if (msgctl(msqid, IPC_RMID, NULL) == -1) {
                perror("msgctl to get rid of queue in parent failed");
                exit(1);
        }

	printf("60 seconds are up! oss.c is now:\n");
	killpg(getpid(), SIGKILL);
	exit(EXIT_SUCCESS);
}


//catches CTRL ^ C, frees up memory and terminates program
static void interruptSig(int sig)
{
	shmdt( clockSec );    // Detach from the shared memory segment
        shmctl( idSec, IPC_RMID, NULL ); // Free shared memory segment shm_id
        shmdt( clockNano );    // Detach from the shared memory segment
        shmctl( idNano, IPC_RMID, NULL ); // Free shared memory segment shm_id
 	if (msgctl(msqid, IPC_RMID, NULL) == -1) {
                perror("msgctl to get rid of queue in parent failed");
                exit(1);
        }

	printf("\nInterrupt Signal CTRL C caught\n");
	exit(EXIT_SUCCESS);
}

//increments clock
void incrementClock(int *sec, int *nano)
{
	if(*nano < 1000000000)
	{
		*nano += 10000000;	//speed of the clock
		if(*nano >= 1000000000)
		{
			*nano -= 1000000000;
	                (*sec)++;
		}
	}
	else
	{
		*nano -= 1000000000;
		(*sec)++;
	}
}


//adds the process' pid, starting sec, starting nanoseconds and resources to a blank spot in the process table
void addProcess(struct PCB pTable[], int pid, int sec, int nano, int R0, int R1, int R2, int R3, int R4, int R5, int R6,int R7, int R8, int R9)
{
	int i;
	for(i = 0; i < 18; i++)
	{
		if(pTable[i].occupied == 0) //if not occupied
		{
			pTable[i].occupied = 1;
			pTable[i].pid = pid;
			pTable[i].startSeconds = sec;
			pTable[i].startNano = nano;	
			pTable[i].R0 = R0;
			pTable[i].R1 = R1;
			pTable[i].R2 = R2;
			pTable[i].R3 = R3;
			pTable[i].R4 = R4;
			pTable[i].R5 = R5;
			pTable[i].R6 = R6;
			pTable[i].R7 = R7;
			pTable[i].R8 = R8;
			pTable[i].R9 = R9;
			pTable[i].blocked = -1;
			return;
		}

	}	
}


//remove process from the process table by searching for the pid
void removeProcess(struct PCB pTable[], int pid)
{
	int i;
	for(i = 0; i < 18; i++)
	{
		if(pTable[i].pid == pid)
		{
			pTable[i].occupied = 0;
                        pTable[i].pid = 0;
                        pTable[i].startSeconds = 0;
                        pTable[i].startNano = 0;
			pTable[i].R0 = 0;
			pTable[i].R1 = 0;
                        pTable[i].R2 = 0;
                        pTable[i].R3 = 0;
                        pTable[i].R4 = 0;
                        pTable[i].R5 = 0;
                        pTable[i].R6 = 0;
                        pTable[i].R7 = 0;
                        pTable[i].R8 = 0;
                        pTable[i].R9 = 0;
 			pTable[i].blocked =-1;
                        return;
		}
	}

}

//prints process table to oss and to logfile
void printTable(struct PCB pTable[], int sec, int nano, FILE * logfile)
{
	int i = 0;
	//printf("OSS PID: %d SysClock: %d SysclockNano: %d\n", getpid(), sec, nano);
	fprintf(logfile, "OSS PID: %d SysClock: %d SysclockNano: %d\n", getpid(), sec, nano);
	//printf("Process Table:\n");
        fprintf(logfile, "Process Table:\n");
	//printf("Entry\tPID\tBlocked\tR0\tR1\tR2\tR3\tR4\tR5\tR6\tR7\tR8\tR9\tStartS\tStartN\n");
        fprintf(logfile, "Entry\tPID\tBlocked\tR0\tR1\tR2\tR3\tR4\tR5\tR6\tR7\tR8\tR9\tStartS\tStartN\n");
	for(i =0; i < 18; i++)
        {
                //printf("P%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", i, pTable[i].pid, pTable[i].blocked, pTable[i].R0, pTable[i].R1, pTable[i].R2, pTable[i].R3, pTable[i].R4, pTable[i].R5, pTable[i].R6, pTable[i].R7, pTable[i].R8, pTable[i].R9, pTable[i].startSeconds, pTable[i].startNano);
        	fprintf(logfile, "P%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", i, pTable[i].pid, pTable[i].blocked, pTable[i].R0, pTable[i].R1, pTable[i].R2, pTable[i].R3, pTable[i].R4, pTable[i].R5, pTable[i].R6, pTable[i].R7, pTable[i].R8, pTable[i].R9, pTable[i].startSeconds, pTable[i].startNano);
	}
        return;
}


//print queues used to visually see queues work
void showq(queue<int> gq, FILE * logfile)
{
    queue<int> g = gq;
    while (!g.empty()) {
        fprintf(logfile, "%d ", g.front());
        g.pop();
    }
	fprintf(logfile, "\n");
}


//prints the available Rs
void Available(int R0, int R1, int R2, int R3, int R4, int R5, int R6, int R7, int  R8, int R9, FILE * logfile)
{
        //printf("AVAILABLE: R0:%d, R1:%d, R2:%d, R3:%d, R4:%d, R5:%d, R6:%d, R7:%d, R8:%d, R9:%d\n", R0, R1, R2, R3, R4, R5, R6, R7, R8, R9);
        fprintf(logfile, "AVAILABLE: R0:%d, R1:%d, R2:%d, R3:%d, R4:%d, R5:%d, R6:%d, R7:%d, R8:%d, R9:%d\n", R0, R1, R2, R3, R4, R5, R6, R7, R8, R9);
}


//return a queue of deadlocked process which will all be send to be killed
queue<int> deadlockDetection(struct PCB pTable[], struct resources available)
{
	//initalize temp table to check for deadlock
	struct PCB tempTable[18];
	for(int i=0; i<18; i++)
	{
                tempTable[i] =pTable[i];
        }
        //temp resource vector
	struct resources tempRA= available;
        //queue that will return deadlocked processes to be removed
	queue<int> deadlockedq;

        //not blocked (-1), remove from table and add to fake available Rs
        for(int i= 0; i<18; i++)
        {
                if(tempTable[i].blocked == -1) //not blocked
                {
                        tempRA.R0 += tempTable[i].R0;
                        tempRA.R1 += tempTable[i].R1;
                        tempRA.R2 += tempTable[i].R2;
                        tempRA.R3 += tempTable[i].R3;
                        tempRA.R4 += tempTable[i].R4;
                        tempRA.R5 += tempTable[i].R5;
                        tempRA.R6 += tempTable[i].R6;
                        tempRA.R7 += tempTable[i].R7;
                        tempRA.R8 += tempTable[i].R8;
                        tempRA.R9 += tempTable[i].R9;
                        removeProcess(tempTable, tempTable[i].pid);
                }
        }
	
	//check all blocked processes
	for(int j = 0; j <18; j++)
	{
		//check to see if we now have the resources available
		for(int i = 0; i<18; i++)
		{
			int n = tempTable[i].blocked;
			switch(n)	//for each case, if we can unblock it, do it, add resources and remove from temp table
			{
				case 0:
				{
					if(tempRA.R0 > 0)
					{
						tempRA.R0 += tempTable[i].R0;
	        	                	tempRA.R1 += tempTable[i].R1;
        		                	tempRA.R2 += tempTable[i].R2;
                		        	tempRA.R3 += tempTable[i].R3;
                	        		tempRA.R4 += tempTable[i].R4;
	        	                	tempRA.R5 += tempTable[i].R5;
        		                	tempRA.R6 += tempTable[i].R6;
                		        	tempRA.R7 += tempTable[i].R7;
	        	                	tempRA.R8 += tempTable[i].R8;
	                	        	tempRA.R9 += tempTable[i].R9;
        	        		        removeProcess(tempTable, tempTable[i].pid);
					} 
					break;
				}
				case 1:
                	        {
               		                if(tempRA.R1 > 0)
                	                {
                	                        tempRA.R0 += tempTable[i].R0;
                        	                tempRA.R1 += tempTable[i].R1;
                               	         	tempRA.R2 += tempTable[i].R2;
                               	         	tempRA.R3 += tempTable[i].R3;
	                                        tempRA.R4 += tempTable[i].R4;
	                                        tempRA.R5 += tempTable[i].R5;
	                                        tempRA.R6 += tempTable[i].R6;
	                                        tempRA.R7 += tempTable[i].R7;
	                                        tempRA.R8 += tempTable[i].R8;
	                                        tempRA.R9 += tempTable[i].R9;
	                                        removeProcess(tempTable, tempTable[i].pid);
	                                }
					break;
	                        }
				case 2:
	                        {
	                                if(tempRA.R2 > 0)
	                                {
	                                        tempRA.R0 += tempTable[i].R0;
	                                        tempRA.R1 += tempTable[i].R1;
	                                        tempRA.R2 += tempTable[i].R2;
	                                        tempRA.R3 += tempTable[i].R3;
	                                        tempRA.R4 += tempTable[i].R4;
	                                        tempRA.R5 += tempTable[i].R5;
	                                        tempRA.R6 += tempTable[i].R6;
	                                        tempRA.R7 += tempTable[i].R7;
	                                        tempRA.R8 += tempTable[i].R8;
	                                        tempRA.R9 += tempTable[i].R9;
	                                        removeProcess(tempTable, tempTable[i].pid);
	                                }
	                        	break;
				}
				case 3:
	                        {
	                                if(tempRA.R3 > 0)
	                                {
	                                        tempRA.R0 += tempTable[i].R0;
	                                        tempRA.R1 += tempTable[i].R1;
	                                        tempRA.R2 += tempTable[i].R2;
	                                        tempRA.R3 += tempTable[i].R3;
	                                        tempRA.R4 += tempTable[i].R4;
	                                        tempRA.R5 += tempTable[i].R5;
	                                        tempRA.R6 += tempTable[i].R6;
	                                        tempRA.R7 += tempTable[i].R7;
	                                        tempRA.R8 += tempTable[i].R8;
	                                        tempRA.R9 += tempTable[i].R9;
	                                        removeProcess(tempTable, tempTable[i].pid);
	                                }
					break;
       	                 	}
				case 4:
       	                 	{
       	                         	if(tempRA.R4 > 0)
       	                        	{
       	                                	tempRA.R0 += tempTable[i].R0;
       	                                	tempRA.R1 += tempTable[i].R1;
                                        	tempRA.R2 += tempTable[i].R2;
	                                        tempRA.R3 += tempTable[i].R3;
	                                        tempRA.R4 += tempTable[i].R4;
	                                        tempRA.R5 += tempTable[i].R5;
	                                        tempRA.R6 += tempTable[i].R6;
	                                        tempRA.R7 += tempTable[i].R7;
	                                        tempRA.R8 += tempTable[i].R8;
	                                        tempRA.R9 += tempTable[i].R9;
	                                        removeProcess(tempTable, tempTable[i].pid);
	                                }
					break;
	                        }
				case 5:
	                        {
	                                if(tempRA.R5 > 0)
	                                {
	                                        tempRA.R0 += tempTable[i].R0;
	                                        tempRA.R1 += tempTable[i].R1;
	                                        tempRA.R2 += tempTable[i].R2;
	                                        tempRA.R3 += tempTable[i].R3;
	                                        tempRA.R4 += tempTable[i].R4;
	                                        tempRA.R5 += tempTable[i].R5;
	                                        tempRA.R6 += tempTable[i].R6;
	                                        tempRA.R7 += tempTable[i].R7;
	                                        tempRA.R8 += tempTable[i].R8;
	                                        tempRA.R9 += tempTable[i].R9;
	                                        removeProcess(tempTable, tempTable[i].pid);
	                                }
	                                break;
	                        }
				case 6:
	                        {
	                                if(tempRA.R6 > 0)
	                                {
	                                        tempRA.R0 += tempTable[i].R0;
	                                        tempRA.R1 += tempTable[i].R1;
	                                        tempRA.R2 += tempTable[i].R2;
	                                        tempRA.R3 += tempTable[i].R3;
	                                        tempRA.R4 += tempTable[i].R4;
	                                        tempRA.R5 += tempTable[i].R5;
	                                        tempRA.R6 += tempTable[i].R6;
	                                        tempRA.R7 += tempTable[i].R7;
	                                        tempRA.R8 += tempTable[i].R8;
	                                        tempRA.R9 += tempTable[i].R9;
	                                        removeProcess(tempTable, tempTable[i].pid);
	                                }
					break;
				}
				case 7:
	                        {
	                                if(tempRA.R7 > 0)
	                                {
	                                        tempRA.R0 += tempTable[i].R0;
	                                        tempRA.R1 += tempTable[i].R1;
	                                        tempRA.R2 += tempTable[i].R2;
	                                        tempRA.R3 += tempTable[i].R3;
	                                        tempRA.R4 += tempTable[i].R4;
      	                                 	tempRA.R5 += tempTable[i].R5;
	                                        tempRA.R6 += tempTable[i].R6;
	                                        tempRA.R7 += tempTable[i].R7;
	                                        tempRA.R8 += tempTable[i].R8;
	                                        tempRA.R9 += tempTable[i].R9;
	                                        removeProcess(tempTable, tempTable[i].pid);
	                                }
	                                break;
	                        }
				case 8:
	                        {
	                                if(tempRA.R8 > 0)
	                                {
	                                        tempRA.R0 += tempTable[i].R0;
	                                        tempRA.R1 += tempTable[i].R1;
	                                        tempRA.R2 += tempTable[i].R2;
	                                        tempRA.R3 += tempTable[i].R3;
	                                        tempRA.R4 += tempTable[i].R4;
	                                        tempRA.R5 += tempTable[i].R5;
	                                        tempRA.R6 += tempTable[i].R6;
	                                        tempRA.R7 += tempTable[i].R7;
	                                        tempRA.R8 += tempTable[i].R8;
	                                        tempRA.R9 += tempTable[i].R9;
	                                        removeProcess(tempTable, tempTable[i].pid);
	                                }
	                                break;
	                        }
				case 9:
	                        {
	                                if(tempRA.R9 > 0)
	                                {
	                                        tempRA.R0 += tempTable[i].R0;
	                                        tempRA.R1 += tempTable[i].R1;
	                                        tempRA.R2 += tempTable[i].R2;
	                                        tempRA.R3 += tempTable[i].R3;
	                                        tempRA.R4 += tempTable[i].R4;
	                                        tempRA.R5 += tempTable[i].R5;
	                                        tempRA.R6 += tempTable[i].R6;
	                                        tempRA.R7 += tempTable[i].R7;
	                                        tempRA.R8 += tempTable[i].R8;
	                                        tempRA.R9 += tempTable[i].R9;
	                                        removeProcess(tempTable, tempTable[i].pid);
	                                }
	                                break;
	                        }			
	
			}
		}
	}
	
	//add whats still deadlocked (left in temp table) to a queue
	for(int i =0; i<18; i++)
	{
		if(tempTable[i].pid != 0)	//not empty aka deadlocked
		{
			deadlockedq.push(tempTable[i].pid);	
		}
	}
	return deadlockedq;	//all are sent to be killed
}

//heart of the oss.cpp program
int main(int argc, char** argv) 
{
	//set up signal catches
	signal(SIGALRM, alarmHandler);
	signal(SIGINT, interruptSig);
	alarm(60);		//timer for termination after 60 real life seconds
	
	struct resources available;		//available resource vector
	available.R0 = MAXRESOURCES;
	available.R1 = MAXRESOURCES;
	available.R2 = MAXRESOURCES;
        available.R3 = MAXRESOURCES;
	available.R4 = MAXRESOURCES;
        available.R5 = MAXRESOURCES;
	available.R6 = MAXRESOURCES;
        available.R7 = MAXRESOURCES;
	available.R8 = MAXRESOURCES;
        available.R9 = MAXRESOURCES;

	printf("Resource Vector: %d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",available.R0, available.R1, available.R2, available.R3, available.R4, available.R5, available.R6, available.R7, available.R8, available.R9); 
	//set up and initalize processTable
	struct PCB processTable[18];
	int j;	
	for(j = 0; j < 18; j++)
	{
		processTable[j].occupied = 0;
		processTable[j].pid = 0;
		processTable[j].startSeconds = 0;
		processTable[j].startNano = 0;
		processTable[j].R0 = 0;
		processTable[j].R1 = 0;
                processTable[j].R2 = 0;
                processTable[j].R3 = 0;
                processTable[j].R4 = 0;
                processTable[j].R5 = 0;
                processTable[j].R6 = 0;
                processTable[j].R7 = 0;
                processTable[j].R8 = 0;
                processTable[j].R9 = 0;
		processTable[j].blocked = -1;
	}


	//allocation:
        keySec = 1234;
        keyNano = 1235;

        idSec = shmget( keySec , sizeof(int)* 2 , IPC_CREAT | 0666  );
        if (idSec <= 0) {
                fprintf(stderr,"Shared memory get failed\n");
                exit(1);
        }

        idNano = shmget( keyNano , sizeof(int)* 2 , IPC_CREAT | 0666  );
        if (idNano <= 0) {
                fprintf(stderr,"Shared memory get failed\n");
                exit(1);
        }

        //clock attaching- get pointer to shared block     
	clockSec = (int*)shmat( idSec, 0 , 0 );
        if (clockSec <= 0) {
                fprintf(stderr,"Shared memory attach failed\n");
                exit(1);
        }

        //clock attaching- get pointer to shared block
        clockNano = (int*)shmat( idNano, 0, 0 );
        if (clockNano <= 0) {
                fprintf(stderr,"Shared memory attach failed\n");
                exit(1);
        }

	//intializing clock with offset of 1 sec
        *clockSec = 1;
        *clockNano = 0;


	//handles the getopt options, and sets the options equal to correct values
	int option;
	int fileFlag = 0;
	int verbose = 1;
	char* fileName;
	FILE * logfile;	

	//only have options -h for help and -f for logfile name
	while((option = getopt(argc, argv, "hf:v")) != -1 )
	{
		switch(option)
		{
			case 'h':
			{
				printf("To run program type: \n./oss -f logfile -v\n\n");
				printf("Where logfile is the optional name of a file and v is use to turn off verbose\n");
				return (EXIT_SUCCESS);
			}
			case 'f':
			{
				fileFlag = 1;
				fileName = optarg;
				break;
			}
			case 'v':
			{
				verbose = 0;
				break;
			}
			default:
			{
				printf("Error. Case not found.\n");
				exit(EXIT_SUCCESS);
			}

		}

	}

	//creates logfile	
	if(fileFlag == 1)	//-f filename given
	{
		if((logfile = fopen(fileName, "w"))==NULL)
		{
			printf("oss: Failed to open file. Sending kill singal.\n");
			kill(getppid(), SIGTERM);
			exit(EXIT_FAILURE);
		}
	}
	else{	//no -f filename given, automatic name "logfile"
		if((logfile = fopen("logfile", "w"))==NULL)
                {
                        printf("oss: Failed to open file. Sending kill singal.\n");
                        kill(getppid(), SIGTERM);
                        exit(EXIT_FAILURE);
                }
	}



	//set up message queue
	msgbuffer buf0;
	key_t key;
	system("touch msgq.txt");

	// get a key for our message queue
	if ((key = ftok("msgq.txt", 1)) == -1) {
		perror("ftok");
		exit(1);
	}

	// create our message queue
	if ((msqid = msgget(key, PERMS | IPC_CREAT)) == -1) {
		perror("msgget in parent");
		exit(1);
	}

	printf("Message queue set up. Please wait while program runs...\n");


        msgbuffer rcvbuf;
	//creates blocked resource queues
	queue<int> blockedqR0;
        queue<int> blockedqR1;
        queue<int> blockedqR2;
        queue<int> blockedqR3;
        queue<int> blockedqR4;
        queue<int> blockedqR5;
        queue<int> blockedqR6;
        queue<int> blockedqR7;
        queue<int> blockedqR8;
        queue<int> blockedqR9;
	int maxTimeBetweenNewProcsNS = 500000000;
	int maxTimeBetweenNewProcsSecs = 0;
	int proc = 40;			//max processes
	int children = 0;
	int terminateChild = 0;
	int choice = 0;		//msg for whats happening
	int decision =0;	//msg .choice 
	int process = 0;	//msg .pid
	int rNum = -1;		//msg .resource
	bool flagoccupied = false;	//check space in processTable
	int nextLaunchSec= 0, nextLaunchNano= 0;
	int totalLines = 0;		//track of logfile lines
	int sec = *clockSec; 	//for deadlock detection	
	//for statistics at the end
	int grantedWaiting = 0;
	int grantedImmediately = 0;
	int successfulTerm = 0;
	int deadlockDetectionRan = 0;
	int deadlockTerm =0;
	int deadlockFound = 0;
	int deadlockNotFound = 0;

	//clock time
	srand(time(NULL));
	maxTimeBetweenNewProcsNS = rand()% (maxTimeBetweenNewProcsNS + 1);	
	maxTimeBetweenNewProcsSecs = rand()% (maxTimeBetweenNewProcsSecs + 1);
	printf("max time between %d:%d\n", maxTimeBetweenNewProcsSecs, maxTimeBetweenNewProcsNS);
	//five second timer
	time_t stime = time(0);
    	long int ctime = stime;
    	long int ntime = stime+5;
	bool fiveSecFlag = false;

	while(terminateChild < proc)		//loop till max processes
	{
		if((grantedWaiting + grantedImmediately) != 0 && ((grantedWaiting + grantedImmediately) % 20 == 0)  && totalLines < 100000 && verbose == 1)	//prints process table, will potentially over 10000 but that fine
                {
                	totalLines += 21;
                        printTable(processTable, *clockSec, *clockNano, logfile);
		}
		
		incrementClock(clockSec, clockNano);
		//check if five seconds have passed, if so flag, else update time
		if(ctime >= ntime && fiveSecFlag == false)
		{
			fiveSecFlag =true;	
			proc = children;
		}
		else if(ctime < ntime)
		{
			ctime =time(0);
		}
		
		//check if time to launch new process
		if((*clockSec > nextLaunchSec ||  *clockSec == nextLaunchSec && *clockNano >= nextLaunchNano) && fiveSecFlag == false)
		{
			nextLaunchSec = maxTimeBetweenNewProcsSecs + *clockSec;
       		        nextLaunchNano = maxTimeBetweenNewProcsNS + *clockNano;
                	if (nextLaunchNano > 1000000000)
                	{
                        	nextLaunchNano -= 1000000000;
                        	nextLaunchSec++;
                	}
			//check if room in process table
			flagoccupied = false;
			for (int i = 0; i < 18; i++)
			{
				if(processTable[i].occupied == 0)
				{	
					flagoccupied = true;
					break;
				}
			}	
			//if room in process table and children forked less than max processes, fork new child
			if(children < proc && flagoccupied == true)
			{
				// lets fork off a child
                		children++; 
				pid_t pid = fork();
				if (pid > 0) {
                        		//add to process table and initalize resources
                        		addProcess(processTable, pid, *clockSec, *clockNano, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
					if(totalLines < 100000 && verbose == 1)
					{
	                			totalLines++;
						//printf("Generating process with PID %d at time %d:%d\n", pid, *clockSec, *clockNano);
         					fprintf(logfile, "Generating process with PID %d at time %d:%d\n", pid, *clockSec, *clockNano);
	
					}
					//uncomment for debugging
                                        //printTable(processTable, *clockSec, *clockNano, logfile);

       				}
                		else if (pid == 0)  
				{
					//send message to exceute
		              		// in child, so lets exec off child executable
	                   		execlp("./user_proc",(char *)NULL);

                        		// should never get here if exec worked
                        		printf("Exec failed for first child\n");
                        		exit(1);
                		}
                		else {
                       			// fork error
         	               		perror("fork failed in parent");
                		}
			}
		}
	
		rcvbuf.pid = -10;		
		// See if message is in msg queue
                if (msgrcv(msqid, &rcvbuf,sizeof(msgbuffer), getpid(), IPC_NOWAIT) == -1) {
               	//printf("No message in Oss\n");
                }

		//stops the termination of already terminated pids		
		if(rcvbuf.pid != -10)
		{	
	
		decision = rcvbuf.choice;	//whatss happening Request, release, terminate;
		rNum = rcvbuf.resource;		//R0- R9
		process = rcvbuf.pid;		//child process pid for lookup in table
		if(decision == 1)	//requesting
		{
			 for(int i = 0; i< 18; i++)	//so root doesnt steal proc
                         {
                              	if(processTable[i].pid == process)
                                {
                               
					switch(rNum)
					{
						case 0:
						{
							if(available.R0 >= 1)	//if we have it grant it
							{
								if(totalLines < 100000){
									//printf("%d is requesting R0 at time %d:%d\n", process, *clockSec, *clockNano);
									totalLines++;
		                                                	fprintf(logfile, "%d is requesting R0 at time %d:%d\n", process, *clockSec, *clockNano);
								}
								buf0.mtype = process;
								if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
			                        		        perror("msgsnd to worker failed\n");
                       				        		exit(1);
		                        			}
                                                                if(totalLines < 100000){
			                                        	//printf("%d granted request R0 at time %d:%d\n", process, *clockSec, *clockNano);
                                                                	totalLines++;
									fprintf(logfile, "%d granted request R0 at time %d:%d\n", process, *clockSec, *clockNano);	
								}
								available.R0 -= 1;
								grantedImmediately++;
								for(int i = 0; i< 18; i++)
								{
									if(processTable[i].pid == process)
									{
										processTable[i].R0 += 1;
										break;
									}
								}
							} 
							else	//else block process
							{
								for(int i = 0; i<18; i++)
								{	
									if(processTable[i].pid == process)
									{
										processTable[i].blocked = 0;
										break;
									}
								}
								blockedqR0.push(process);
                                                                if(totalLines < 100000 && verbose == 1){
									//printf("%d is blocked on R0 at %d:%d\n", process, *clockSec, *clockNano);
									totalLines++;								
                                                                	fprintf(logfile, "%d is blocked on R0 at %d:%d\n", process, *clockSec, *clockNano);
 								}

							}	
							break;	
						}
						case 1:
                                                {
                                                        if(available.R1 >= 1)   //if we have it grant it
                                                        {
                                                                if(totalLines < 100000){
									//printf("%d is requesting R1 at time %d:%d\n", process, *clockSec, *clockNano);
                                                                	totalLines++;
									fprintf(logfile, "%d is requesting R1 at time %d:%d\n", process, *clockSec, *clockNano);
								}
								buf0.mtype = process;
                                                                if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                                        perror("msgsnd to worker failed\n");
                                                                        exit(1);
                                                                }
                                                                if(totalLines < 100000){
                                                                	//printf("%d granted request R1 at time %d:%d\n", process, *clockSec, *clockNano);
                                                                	totalLines++;
                                                                	fprintf(logfile, "%d granted request R1 at time %d:%d\n", process, *clockSec, *clockNano);
								}
								available.R1 -= 1;
                                                		grantedImmediately++; 
						                for(int i = 0; i< 18; i++)
                                                                {
                                                                        if(processTable[i].pid == process)
                                                                        {
                                                                                processTable[i].R1 += 1;
                                                                                break;
                                                                        }
                                                                }
                                                        }
                                                        else    //else block process
                                                        {
                                                                for(int i = 0; i<18; i++)
                                                                {
                                                                        if(processTable[i].pid == process)
                                                                        {
                                                                                processTable[i].blocked = 1;
                                                                		break;
								        }
                                                                }
                                                                blockedqR1.push(process);
                                                                if(totalLines < 100000 && verbose == 1){
									//printf("%d is blocked on R1 at %d:%d\n", process, *clockSec, *clockNano);
									totalLines++;
                                                         	       	fprintf(logfile,"%d is blocked on R1 at %d:%d\n", process, *clockSec, *clockNano);
								}
							}
                                                        break;
                                                }
						case 2:
                                                {
                                                        if(available.R2 >= 1)   //if we have it grant it
                                                        {                                                                
                                                                if(totalLines < 100000){
									//printf("%d is requesting R2 at time %d:%d\n", process, *clockSec, *clockNano);
                                                 			totalLines++;
									fprintf(logfile, "%d is requesting R2 at time %d:%d\n", process, *clockSec, *clockNano);
						                }
								buf0.mtype = process;
                                                                if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                                        perror("msgsnd to worker failed\n");
                                                                        exit(1);
                                                                }
                                                                if(totalLines < 100000){
                                                                	//printf("%d granted request R2 at time %d:%d\n", process, *clockSec, *clockNano);
									totalLines++;
                                                                	fprintf(logfile, "%d granted request R2 at time %d:%d\n", process, *clockSec, *clockNano);
								}
								available.R2 -= 1;
                                                                grantedImmediately++;
                                                                for(int i = 0; i< 18; i++)
                                                                {
                                                                        if(processTable[i].pid == process)
                                                                        {
                                                                                processTable[i].R2 += 1;
                                                                                break;
                                                                        }
                                                                }
                                                        }
                                                        else    //else block process
                                                        {
                                                                for(int i = 0; i<18; i++)
                                                                {
                                                                        if(processTable[i].pid == process)
                                                                        {
                                                                                processTable[i].blocked = 2;
                                        					break; 
					                                }
                                                                }
                                                                blockedqR2.push(process);
                                                                if(totalLines < 100000 && verbose ==1 ){
									//printf("%d is blocked on R2 at time %d:%d\n", process, *clockSec, *clockNano);
									totalLines++;
                                                 			fprintf(logfile, "%d is blocked on R2 at time %d:%d\n", process, *clockSec, *clockNano);	
						        	}
							}
							break;
                                                }
						case 3:
                                                {
                                                        if(available.R3 >= 1)   //if we have it grant it
                                                        {
                                                                if(totalLines < 100000){
									//printf("%d is requesting R3 at time %d:%d\n", process, *clockSec, *clockNano);
                                                 			totalLines++;
									fprintf(logfile, "%d is requesting R3 at time %d:%d\n", process, *clockSec, *clockNano);
						                }
								buf0.mtype = process;
                                                                if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                                        perror("msgsnd to worker failed\n");
                                                                        exit(1);
                                                                }
                                                                if(totalLines < 100000){
                                                                	//printf("%d granted request R3 at time %d:%d\n", process, *clockSec, *clockNano);
                                                                	totalLines++;
									fprintf(logfile, "%d granted request R3 at time %d:%d\n", process, *clockSec, *clockNano);	
								}
								available.R3 -= 1;
                                                                grantedImmediately++;
                                                                for(int i = 0; i< 18; i++)
                                                                {
                                                                        if(processTable[i].pid == process)
                                                                        {
                                                                                processTable[i].R3 += 1;
                                                                                break;
                                                                        }
                                                                }
                                                        }
                                                        else    //else block process
                                                        {
                                                                for(int i = 0; i<18; i++)
                                                                {
                                                                        if(processTable[i].pid == process)
                                                                        {
                                                                                processTable[i].blocked = 3;
                                                 				break;
						                        }
                                                                }
                                                                blockedqR3.push(process);
                                                                if(totalLines < 100000 && verbose == 1){
									//printf("%d is blocked on R3 at time %d:%d\n", process, *clockSec, *clockNano);
									totalLines++;
									fprintf(logfile, "%d is blocked on R3 at time %d:%d\n", process, *clockSec, *clockNano);
								}
							}
                                                        break;
                                                }
						case 4:
                                                {
                                                        if(available.R4 >= 1)   //if we have it grant it
                                                        {
                                                                if(totalLines < 100000){
									//printf("%d is requesting R4 at time %d:%d\n", process, *clockSec, *clockNano);
                                                                	totalLines++;
                                                                	fprintf(logfile, "%d is requesting R4 at time %d:%d\n", process, *clockSec, *clockNano);
								}
								buf0.mtype = process;
                                                                if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                                        perror("msgsnd to worker failed\n");
                                                                        exit(1);
                                                                }
                                                                if(totalLines < 100000){
	                                                                //printf("%d granted request R4 at time %d:%d\n", process, *clockSec, *clockNano);
                                                                	totalLines++;
							        	fprintf(logfile, "%d granted request R4 at time %d:%d\n", process, *clockSec, *clockNano);	
								}
								available.R4 -= 1;
                                                                grantedImmediately++;
                                                                for(int i = 0; i< 18; i++)
                                                                {
                                                                        if(processTable[i].pid == process)
                                                                        {
                                                                                processTable[i].R4 += 1;
                                                                                break;
                                                                        }
                                                                }
                                                        }
                                                        else    //else block process
                                                        {
                                                                for(int i = 0; i<18; i++)
                                                                {
                                                                        if(processTable[i].pid == process)
                                                                        {
                                                                                processTable[i].blocked = 4;
                                                				break;
						                        }
                                                                }
                                                                blockedqR4.push(process);
                                                                if(totalLines < 100000 && verbose == 1){
									//printf("%d is blocked on R4 at time %d:%d\n", process, *clockSec, *clockNano);
									totalLines++;
                                                                	fprintf(logfile, "%d is blocked on R4 at time %d:%d\n", process, *clockSec, *clockNano);	
						        	}	
							}
                                                        break;
                                                }
						case 5:
                                                {
                                                        if(available.R5 >= 1)   //if we have it grant it
                                                        {
                                                                if(totalLines < 100000){
									//printf("%d is requesting R5 at time %d:%d\n", process, *clockSec, *clockNano);
                                                                	totalLines++;
									fprintf(logfile, "%d is requesting R5 at time %d:%d\n", process, *clockSec, *clockNano);	
								}
								buf0.mtype = process;
                                                                if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                                        perror("msgsnd to worker failed\n");
                                                                        exit(1);
                                                                }
                                                                if(totalLines < 100000){
                                                                	//printf("%d granted request R5 at time %d:%d\n", process, *clockSec, *clockNano);
                                                                	totalLines++;
									fprintf(logfile, "%d granted request R5 at time %d:%d\n", process, *clockSec, *clockNano);	
								}
								available.R5 -= 1;
                                                                grantedImmediately++;
                                                                for(int i = 0; i< 18; i++)
                                                                {
                                                                        if(processTable[i].pid == process)
                                                                        {
                                                                                processTable[i].R5 += 1;
                                                                                break;
                                                                        }
                                                                }
                                                        }
                                                        else    //else block process
                                                        {
                                                                for(int i = 0; i<18; i++)
                                                                {
                                                                        if(processTable[i].pid == process)
                                                                        {
                                                                                processTable[i].blocked = 5;
                                                 				break;
						                        }
                                                                }
 
                                                                blockedqR5.push(process);
                                                                if(totalLines < 100000 && verbose == 1){
									//printf("%d is blocked on R5 at time %d:%d\n", process, *clockSec, *clockNano);
	 								totalLines++;
									fprintf(logfile, "%d is blocked on R5 at time %d:%d\n", process, *clockSec, *clockNano);	
						        	}
							}
							break;
                                                }
						case 6:
                                                {
                                                        if(available.R6 >= 1)   //if we have it grant it
                                                        {
                                                                if(totalLines < 100000){
									//printf("%d is requesting R6 at time %d:%d\n", process, *clockSec, *clockNano);
                                                                	totalLines++;
									fprintf(logfile, "%d is requesting R6 at time %d:%d\n", process, *clockSec, *clockNano);
								}
								buf0.mtype = process;
                                                                if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                                        perror("msgsnd to worker failed\n");
                                                                        exit(1);
                                                                }
                                                                if(totalLines < 100000){
									//printf("%d granted request R6 at time %d:%d\n", process, *clockSec, *clockNano);
                                                                	totalLines++;
									fprintf(logfile, "%d granted request R6 at time %d:%d\n", process, *clockSec, *clockNano);
								}
								available.R6 -= 1;
                                                                grantedImmediately++;
                                                                for(int i = 0; i< 18; i++)
                                                                {
                                                                        if(processTable[i].pid == process)
                                                                        {
                                                                                processTable[i].R6 += 1;
                                                                                break;
                                                                        }
                                                                }
                                                        }
                                                        else    //else block process
                                                        {
                                                                for(int i = 0; i<18; i++)
                                                                {
                                                                        if(processTable[i].pid == process)
                                                                        {
                                                                                processTable[i].blocked = 6;
                                                 				break;
						                        }
                                                                }
                                                                blockedqR6.push(process);
                                                                if(totalLines < 100000 && verbose == 1){
									//printf("%d is blocked on R6 at time %d:%d\n", process, *clockSec, *clockNano);
									totalLines++;
							        	fprintf(logfile, "%d is blocked on R6 at time %d:%d\n", process, *clockSec, *clockNano);	
								}	
							}
						        break;
                                                }
						case 7:
                                                {
                                                        if(available.R7 >= 1)   //if we have it grant it
                                                        {
                                                                if(totalLines < 100000){
									//printf("%d is requesting R7 at time %d:%d\n", process, *clockSec, *clockNano);
                                                 			totalLines++;
									fprintf(logfile, "%d is requesting R7 at time %d:%d\n", process, *clockSec, *clockNano);
						                }
								buf0.mtype = process;
                                                                if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                                        perror("msgsnd to worker failed\n");
                                                                        exit(1);
                                                                }
                                                                if(totalLines < 100000){
                                                                	//printf("%d granted request R7 at time %d:%d\n", process, *clockSec, *clockNano);
                                                                	totalLines++;
                                                                	fprintf(logfile, "%d granted request R7 at time %d:%d\n", process, *clockSec, *clockNano);
								}
								available.R7 -= 1;
                                                                grantedImmediately++;
                                                                for(int i = 0; i< 18; i++)
                                                                {
                                                                        if(processTable[i].pid == process)

                                                                        {
                                                                                processTable[i].R7 += 1;
                                                                                break;
                                                                        }
                                                                }
                                                        }
                                                        else    //else block process
                                                        {
                                                                for(int i = 0; i<18; i++)
                                                                {
                                                                        if(processTable[i].pid == process)
                                                                        {
                                                                                processTable[i].blocked = 7;
                                                				break;
						                        }
                                                                }
                                                                blockedqR7.push(process);
                                                                if(totalLines < 100000 && verbose == 1){
									//printf("%d is blocked on R7 at time %d:%d\n", process, *clockSec, *clockNano);
									totalLines++;
                                                                	fprintf(logfile, "%d is blocked on R7 at time %d:%d\n", process, *clockSec, *clockNano);
								}
							}	
                                                        break;
                                                }
						case 8:
                                                {
                                                        if(available.R8 >= 1)   //if we have it grant it
                                                        {
                                                                if(totalLines < 100000){
									//printf("%d is requesting R8 at time %d:%d\n", process, *clockSec, *clockNano);
                                                	 		totalLines++;
                                                                	fprintf(logfile, "%d is requesting R8 at time %d:%d\n", process, *clockSec, *clockNano);
								}
								buf0.mtype = process;
                                                                if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){

                                                                        perror("msgsnd to worker failed\n");
                                                                        exit(1);
                                                                }
                                                                if(totalLines < 100000){
									//printf("%d granted request R8 at time %d:%d\n", process, *clockSec, *clockNano);
                                                                	totalLines++;
									fprintf(logfile, "%d granted request R8 at time %d:%d\n", process, *clockSec, *clockNano);	
								}
								available.R8 -= 1;
                                                                grantedImmediately++;
                                                                for(int i = 0; i< 18; i++)
                                                                {
                                                                        if(processTable[i].pid == process)
                                                                        {
                                                                                processTable[i].R8 += 1;
                                                                                break;
                                                                        }
                                                                }
                                                        }
                                                        else    //else block process
                                                        {
                                                                for(int i = 0; i<18; i++)
                                                                {
                                                                        if(processTable[i].pid == process)
                                                                        {
                                                                                processTable[i].blocked = 8;
                                                				break;
						                        }
                                                                }
                                                                blockedqR8.push(process);
                                                                if(totalLines < 100000 && verbose == 1){
									//printf("%d is blocked on R8 at time %d:%d\n", process, *clockSec, *clockNano);
						        		totalLines++;
                                                                	fprintf(logfile, "%d is blocked on R8 at time %d:%d\n", process, *clockSec, *clockNano);
								}
							}
                                                        break;
                                                }
						case 9:
                                                {
                                                        if(available.R9 >= 1)   //if we have it grant it
                                                        {
                                                                if(totalLines < 100000){
									//printf("%d is requesting R9 at time %d:%d\n", process, *clockSec, *clockNano);
                                                                	totalLines++;
                                                                	fprintf(logfile, "%d is requesting R9 at time %d:%d\n", process, *clockSec, *clockNano);
								}
								buf0.mtype = process;
                                                                if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                                        perror("msgsnd to worker failed\n");
                                                                        exit(1);
                                                                }
                                                                if(totalLines < 100000){
                                                                	//printf("%d granted request R9 at time %d:%d\n", process, *clockSec, *clockNano);
                                                                	totalLines++;
                                                                	fprintf(logfile, "%d granted request R9 at time %d:%d\n", process, *clockSec, *clockNano);
								}
								available.R9 -= 1;
                                                                grantedImmediately++;
                                                                for(int i = 0; i< 18; i++)
                                                                {
                                                                        if(processTable[i].pid == process)
                                                                        {
                                                                                processTable[i].R9 += 1;
                                                                                break;
                                                                        }
                                                                }
                                                        }
                                                        else    //else block process
                                                        {
                                                                for(int i = 0; i<18; i++)
                                                                {
                                                                        if(processTable[i].pid == process)
                                                                        {
                                                                                processTable[i].blocked = 9;
                                                				break;
						                        }
                                                                }
                                                                blockedqR9.push(process);
                                                                if(totalLines < 100000  && verbose == 1){
									//printf("%d is blocked on R9 at time %d:%d\n", process, *clockSec, *clockNano);
				        				totalLines++;
                                                                	fprintf(logfile, "%d is blocked on R9 at time %d:%d\n", process, *clockSec, *clockNano);
								}	 
					               }
                                                       break;
                                                }
					}
                                        if(totalLines < 100000){
				 		totalLines++;
						Available(available.R0, available.R1, available.R2, available.R3, available.R4, available.R5, available.R6, available.R7, available.R8, available.R9, logfile);
                                        }
					//printTable(processTable, *clockSec, *clockNano, logfile);
	
				}
			}
		}
		else if(decision == 2) 	//releasing
		{
                	switch(rNum)
			{
				case 0:
				{
					//send msg to worker to show msg was sucessful and continue
                			buf0.mtype = process;
                                        if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                perror("msgsnd to worker failed\n");
                                                exit(1);
                                        }
                                        if(totalLines < 100000 && verbose == 1){
						//printf("%d is releasing R0 at time %d:%d\n\tResources released: R0:1\n", process, *clockSec, *clockNano);
						totalLines++;
                                        	fprintf(logfile, "%d is releasing R0 at time %d:%d\n\tResources released: R0:1\n", process, *clockSec, *clockNano);	
					}
					for(int i = 0; i< 18; i++)                                                                                             	
					{
                                               	if(processTable[i].pid == process)
                                               	{
                                               		processTable[i].R0 -= 1;
                                                        break;
                                       		}
					}
		                        available.R0 += 1;	
					break;			
				}
				case 1:
                                {
                                        //send msg to worker to show msg was sucessful and continue
                                        buf0.mtype = process;
                                        if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                perror("msgsnd to worker failed\n");
                                                exit(1);
                                        }
                                        if(totalLines < 100000 && verbose == 1){
                                        	//printf("%d is releasing R1 at time %d:%d\n\tResources released: R1:1\n", process, *clockSec, *clockNano);
                                        	totalLines++;
                                        	fprintf(logfile, "%d is releasing R1 at time %d:%d\n\tResources released: R1:1\n", process, *clockSec, *clockNano);
					}
					for(int i = 0; i< 18; i++)                                                                                             
					{
                                                if(processTable[i].pid == process)
                                                {
                                                        processTable[i].R1 -= 1;
                                                        break;
                                                }
                                        }
                                        available.R1 += 1;
                                        break;
                                }
				case 2:
                                {
                                        //send msg to worker to show msg was sucessful and continue
                                        buf0.mtype = process;
                                        if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                perror("msgsnd to worker failed\n");
                                                exit(1);
                                        }
                                        if(totalLines < 100000 && verbose == 1){                                        
						//printf("%d is releasing R2 at time %d:%d\n\tResources released: R2:1\n", process, *clockSec, *clockNano);
                                        	totalLines++;
                                        	fprintf(logfile, "%d is releasing R2 at time %d:%d\n\tResources released: R2:1\n", process, *clockSec, *clockNano);
					}
					for(int i = 0; i< 18; i++)                                                                                             
					{
                                                if(processTable[i].pid == process)
                                                {
                                                        processTable[i].R2 -= 1;
                                                        break;
                                                }
                                        }
                                        available.R2 += 1;
                                        break;
                                }
				case 3:
                                {
                                        //send msg to worker to show msg was sucessful and continue
                                        buf0.mtype = process;
                                        if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                perror("msgsnd to worker failed\n");
                                                exit(1);
                                        }
                                        if(totalLines < 100000 && verbose == 1){
						//printf("%d is releasing R3 at time %d:%d\n\tResources released: R3:1\n", process, *clockSec, *clockNano);
                                        	totalLines++;
                                        	fprintf(logfile, "%d is releasing R3 at time %d:%d\n\tResources released: R3:1\n", process, *clockSec, *clockNano);
					}
					for(int i = 0; i< 18; i++)                                                                                             
					{
                                                if(processTable[i].pid == process)
                                                {
                                                        processTable[i].R3 -= 1;
                                                        break;
                                                }
                                        }
                                        available.R3 += 1;
                                        break;
                                }
				case 4:
                                {
                                        //send msg to worker to show msg was sucessful and continue
                                        buf0.mtype = process;
                                        if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                perror("msgsnd to worker failed\n");
                                                exit(1);
                                        }
                                        if(totalLines < 100000 && verbose == 1){
						//printf("%d is releasing R4 at time %d:%d\n\tResources released: R4:1\n", process, *clockSec, *clockNano);
                                        	totalLines++;
                                        	fprintf(logfile, "%d is releasing R4 at time %d:%d\n\tResources released: R4:1\n", process, *clockSec, *clockNano);
					}
					for(int i = 0; i< 18; i++)                                                                                             
					{
                                                if(processTable[i].pid == process)
                                                {
                                                        processTable[i].R4 -= 1;
                                                        break;
                                                }
                                        }
                                        available.R4 += 1;
                                        break;
                                }
				case 5:
                                {
                                        //send msg to worker to show msg was sucessful and continue
                                        buf0.mtype = process;
                                        if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                perror("msgsnd to worker failed\n");
                                                exit(1);
                                        }
                                        if(totalLines < 100000 && verbose == 1){
						//printf("%d is releasing R5 at time %d:%d\n\tResources released: R5:1\n", process, *clockSec, *clockNano);
                                        	totalLines++;
                                        	fprintf(logfile, "%d is releasing R5 at time %d:%d\n\tResources released: R5:1\n", process, *clockSec, *clockNano);
					}
					for(int i = 0; i< 18; i++)                                                                                             
					{
                                                if(processTable[i].pid == process)
                                                {
                                                        processTable[i].R5 -= 1;
                                                        break;
                                                }
                                        }
                                        available.R5 += 1;
                                        break;
                                }case 6:
                                {
                                        //send msg to worker to show msg was sucessful and continue
                                        buf0.mtype = process;
                                        if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                perror("msgsnd to worker failed\n");
                                                exit(1);
                                        }
                                        if(totalLines < 100000 && verbose == 1){
						//printf("%d is releasing R6 at time %d:%d\n\tResources released: R6:1\n", process, *clockSec, *clockNano);
                                        	totalLines++;
                                        	fprintf(logfile, "%d is releasing R6 at time %d:%d\n\tResources released: R6:1\n", process, *clockSec, *clockNano);
					}
					for(int i = 0; i< 18; i++)                                                                                             
					{
                                                if(processTable[i].pid == process)
                                                {
                                                        processTable[i].R6 -= 1;
                                                        break;
                                                }
                                        }
                                        available.R6 += 1;
                                        break;
                                }case 7:
                                {
                                        //send msg to worker to show msg was sucessful and continue
                                        buf0.mtype = process;
                                        if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                perror("msgsnd to worker failed\n");
                                                exit(1);
                                        }
                                        if(totalLines < 100000 && verbose == 1){
						//printf("%d is releasing R7 at time %d:%d\n\tResources released: R7:1\n", process, *clockSec, *clockNano);
                                        	totalLines++;
                                        	fprintf(logfile, "%d is releasing R7 at time %d:%d\n\tResources released: R7:1\n", process, *clockSec, *clockNano);
					}
					for(int i = 0; i< 18; i++)                                                                                    					 {
                                                if(processTable[i].pid == process)
                                                {
                                                        processTable[i].R7 -= 1;
                                                        break;
                                                }
                                        }
                                        available.R7 += 1;
                                        break;
                                }case 8:
                                {
                                        //send msg to worker to show msg was sucessful and continue
                                        buf0.mtype = process;
                                        if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                perror("msgsnd to worker failed\n");
                                                exit(1);
                                        }
                                        if(totalLines < 100000 && verbose == 1){
						//printf("%d is releasing R8 at time %d:%d\n\tResources released: R8:1\n", process, *clockSec, *clockNano);
                                        	totalLines++;
                                        	fprintf(logfile, "%d is releasing R8 at time %d:%d\n\tResources released: R8:1\n", process, *clockSec, *clockNano);
					}
					for(int i = 0; i< 18; i++)                                                                                             
					{
                                                if(processTable[i].pid == process)
                                                {
                                                        processTable[i].R8 -= 1;
                                                        break;
                                                }
                                        }
                                        available.R8 += 1;
                                        break;
                                }case 9:
                                {
                                        //send msg to worker to show msg was sucessful and continue
                                        buf0.mtype = process;
                                        if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
                                                perror("msgsnd to worker failed\n");
                                                exit(1);
                                        }
                                        if(totalLines < 100000 && verbose == 1){
						//printf("%d is releasing R9 at time %d:%d\n\tResources released: R9:1\n", process, *clockSec, *clockNano);
                                        	totalLines++;
                                        	fprintf(logfile, "%d is releasing R8 at time %d:%d\n\tResources released: R8:1\n", process, *clockSec, *clockNano);
					}
					for(int i = 0; i< 18; i++)                                                                                             
					{
                                                if(processTable[i].pid == process)
                                                {
                                                        processTable[i].R9 -= 1;
                                                        break;
                                                }
                                        }
                                        available.R9 += 1;
                                        break;
                                }

			}
			if(totalLines < 100000){
				Available(available.R0, available.R1, available.R2, available.R3, available.R4, available.R5, available.R6, available.R7, available.R8, available.R9, logfile);
                        }
			//printTable(processTable, *clockSec, *clockNano, logfile);                
                }	
		else if(decision == 3)	//terminating
		{
			
      	                if(totalLines < 100000 && verbose == 1){
				//printf("PID %d terminated. Resources released : ", process);
                        	totalLines++;
				fprintf(logfile, "PID %d terminated. Resources released : ", process);
			}
			buf0.mtype = process;
			if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
       	                        perror("msgsnd to worker failed\n");
                                exit(1);
                        }
			for(int i = 0; i < 18; i++)
			{
				if(processTable[i].pid == process)
				{
                                        if(totalLines < 100000 && verbose == 1){
						//printf("R0:%d, R1:%d, R2:%d, R3:%d, R4:%d, R5:%d, R6:%d, R7:%d, R8:%d, R9:%d\n ", processTable[i].R0, processTable[i].R1,processTable[i].R2,processTable[i].R3, processTable[i].R4, processTable[i].R5, processTable[i].R6, processTable[i].R7, processTable[i].R8, processTable[i].R9 );
						totalLines++;
						fprintf(logfile, "R0:%d, R1:%d, R2:%d, R3:%d, R4:%d, R5:%d, R6:%d, R7:%d, R8:%d, R9:%d\n ", processTable[i].R0, processTable[i].R1,processTable[i].R2,processTable[i].R3, processTable[i].R4, processTable[i].R5, processTable[i].R6, processTable[i].R7, processTable[i].R8, processTable[i].R9 );
                                        }
					available.R0 += processTable[i].R0;
                                        available.R1 += processTable[i].R1;
					available.R2 += processTable[i].R2;
                                        available.R3 += processTable[i].R3;
					available.R4 += processTable[i].R4;
                                        available.R5 += processTable[i].R5;
					available.R6 += processTable[i].R6;
                                        available.R7 += processTable[i].R7;
					available.R8 += processTable[i].R8;
                                        available.R9 += processTable[i].R9;
					break;
        			}        
			}			
                        if(totalLines < 100000 && verbose == 1){
				totalLines++;
				fprintf(logfile, "\n");
			}
                        if(totalLines < 100000){
				totalLines++;
				Available(available.R0, available.R1, available.R2, available.R3, available.R4, available.R5, available.R6, available.R7, available.R8, available.R9, logfile); 
			}
 		        removeProcess(processTable, process);
                        //printTable(processTable, *clockSec, *clockNano, logfile);
			successfulTerm++;
			terminateChild++;
                        wait(NULL);
                }
		}
  		bool foundR0 = false;
		//check blockedqs to be "unblocked"
		if(blockedqR0.size() > 0 && available.R0 > 0)
		{	
			for(int i = 0; i<18; i++){
				if(blockedqR0.front() == processTable[i].pid)	//if in process table, not terminated
				{
					foundR0 = true;
					break;
				}
			}
			if(foundR0 == true){			
				buf0.mtype = blockedqR0.front();
	                        if(totalLines < 100000 && verbose == 1){
					//printf("Unblocking PID %d by granting request R0 at time %d:%d\n", blockedqR0.front(), *clockSec, *clockNano);
					totalLines++;
	                        	fprintf(logfile, "Unblocking PID %d by granting request R0 at time %d:%d\n", blockedqR0.front(), *clockSec, *clockNano);	
				}
				if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
	                     	      	perror("msgsnd to worker failed\n");
	                     		exit(1);
	              		}
				for(int i = 0; i< 18; i++)                                                               				
				{
					if(processTable[i].pid == blockedqR0.front())
	             			{
	                               	 	processTable[i].R0 += 1;
	                 			processTable[i].blocked = -1;
	                 		        break;
	                      		}
	                       	}	
				blockedqR0.pop();
				available.R0 -= 1;
				grantedWaiting++;
				if(totalLines < 100000){
					totalLines++;
					Available(available.R0, available.R1, available.R2, available.R3, available.R4, available.R5, available.R6, available.R7, available.R8, available.R9, logfile);
				}
				//printTable(processTable, *clockSec, *clockNano, logfile);
			}
			else
			{	
				blockedqR0.pop();
			}
		}
	
		//check blockedq1 to be "unblocked"
                bool foundR1 = false;
		if(blockedqR1.size() > 0 && available.R1 > 0)
                {
                	for(int i = 0; i<18; i++){	
                                if(blockedqR1.front() == processTable[i].pid)   //if in process table, not terminated
                                {
                                        foundR1 = true;
                                        break;
                                }
                        }
			if(foundR1 == true){
				buf0.mtype = blockedqR1.front();
                        	if(totalLines < 100000 && verbose == 1){
                        		//printf("Unblocking PID %d by granting request R1 at time %d:%d\n", blockedqR1.front(), *clockSec, *clockNano);
                       			totalLines++;
                       			fprintf(logfile, "Unblocking PID %d by granting request R1 at time %d:%d\n", blockedqR1.front(), *clockSec, *clockNano);	
				}
				if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
	                        	perror("msgsnd to worker failed\n");
	                                exit(1);
	                        }
	                        for(int i = 0; i< 18; i++)                                          
	     			{
	                              	if(processTable[i].pid == blockedqR1.front())
	                                {
	                                        processTable[i].R1 += 1;
	                			processTable[i].blocked = -1;
			                        break;
	                                }
	                        }
	                        blockedqR1.pop();
	                        available.R1 -= 1;
	                        grantedWaiting++;
                                if(totalLines < 100000){
					totalLines++;
					Available(available.R0, available.R1, available.R2, available.R3, available.R4, available.R5, available.R6, available.R7, available.R8, available.R9, logfile);
	                        }
				// printTable(processTable, *clockSec, *clockNano, logfile);
	                }
			else
			{
				blockedqR1.pop();
			}
		}	
		//check blockedq2 to be "unblocked"
                bool foundR2 = false;
                if(blockedqR2.size() > 0 && available.R2 > 0)
                {
	                for(int i = 0; i<18; i++){
                                if(blockedqR2.front() == processTable[i].pid)   //if in process table, not terminated
                                {
                                        foundR2 = true;
                                        break;
                                }
                        }
			if(foundR2 == true){
				buf0.mtype = blockedqR2.front();
                                if(totalLines < 100000 && verbose == 1){
		                        //printf("Unblocking PID %d by granting request R2 at time %d:%d\n", blockedqR2.front(), *clockSec, *clockNano);
		                        totalLines++;
		                        fprintf(logfile, "Unblocking PID %d by granting request R2 at time %d:%d\n", blockedqR2.front(), *clockSec, *clockNano);
				}
				if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
	                        	perror("msgsnd to worker failed\n");
	                                exit(1);
	                        }
	                        for(int i = 0; i< 18; i++)                                                                                              
				{
	                        	if(processTable[i].pid == blockedqR2.front())
	                                {
	                                	processTable[i].R2 += 1;
	                                        processTable[i].blocked = -1; 
						break;
	                                }
        	                }
	                        blockedqR2.pop();
	                        available.R2 -= 1;
	                        grantedWaiting++;
                                if(totalLines < 100000){
					totalLines++;
		                        Available(available.R0, available.R1, available.R2, available.R3, available.R4, available.R5, available.R6, available.R7, available.R8, available.R9, logfile);
                        	}
				//printTable(processTable, *clockSec, *clockNano, logfile);
                	}
                        else
                        {
                                blockedqR2.pop();
                        }

		}
		//check blockedq3 to be "unblocked"
                bool foundR3 = false;
                if(blockedqR3.size() > 0 && available.R3 > 0)
                {
		        for(int i = 0; i<18; i++){
                                if(blockedqR3.front() == processTable[i].pid)   //if in process table, not terminated
                                {
                                        foundR3 = true;
                                        break;
                                }
                        }
			if(foundR3 == true){
	                	buf0.mtype = blockedqR3.front();
                                if(totalLines < 100000 && verbose == 1){
		                        //printf("Unblocking PID %d by granting request R3 at time %d:%d\n", blockedqR3.front(), *clockSec, *clockNano);
	                        	totalLines++;
		                        fprintf(logfile, "Unblocking PID %d by granting request R3 at time %d:%d\n", blockedqR3.front(), *clockSec, *clockNano);
				}
	                        if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
	                                perror("msgsnd to worker failed\n");
	                                exit(1);
	                        }
	                        for(int i = 0; i< 18; i++)                                                                                              
				{
	                                if(processTable[i].pid == blockedqR3.front())
	                                {
	                                         processTable[i].R3 += 1;
						 processTable[i].blocked = -1;
	                                         break;
	                                }
	                        }
	                        blockedqR3.pop();
	                        available.R3 -= 1;
	                        grantedWaiting++;
                                if(totalLines < 100000){
					totalLines++;
	             	                Available(available.R0, available.R1, available.R2, available.R3, available.R4, available.R5, available.R6, available.R7, available.R8, available.R9, logfile);
                 	        }
				//printTable(processTable, *clockSec, *clockNano, logfile);
                	}
                        else
                        {
                                blockedqR3.pop();
                        }

		}
		//check blockedq4 to be "unblocked"
                bool foundR4 = false;
                if(blockedqR4.size() > 0 && available.R4 > 0)
                {
                	for(int i = 0; i<18; i++){
                                if(blockedqR4.front() == processTable[i].pid)   //if in process table, not terminated
                                {
                                        foundR4 = true;
                                        break;
                                }
                        }
			if(foundR4 == true){
			        buf0.mtype = blockedqR4.front();
                                if(totalLines < 100000 && verbose == 1){
x
		                        //printf("Unblocking PID %d by granting request R4 at time %d:%d\n", blockedqR4.front(), *clockSec, *clockNano);
		                        totalLines++;
		                        fprintf(logfile, "Unblocking PID %d by granting request R4 at time %d:%d\n", blockedqR4.front(), *clockSec, *clockNano);
	                        }
				if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
	                                 perror("msgsnd to worker failed\n");
	                                 exit(1);
	                        }
	                        for(int i = 0; i< 18; i++)                                                                                              
				{
	                                 if(processTable[i].pid == blockedqR4.front())
	                                 {
	                                        processTable[i].R4 += 1;
	                			processTable[i].blocked = -1; 
			                        break;
	                                 }
	                        }
	                        blockedqR4.pop();
	                        available.R4 -= 1;
	                        grantedWaiting++;
                                if(totalLines < 100000){
					totalLines++;
					Available(available.R0, available.R1, available.R2, available.R3, available.R4, available.R5, available.R6, available.R7, available.R8, available.R9, logfile);
	                        }
				//printTable(processTable, *clockSec, *clockNano, logfile);
	                }
                        else
                        {
       	                        blockedqR4.pop();
       	                }
		}
		//check blockedq5 to be "unblocked"
                bool foundR5 = false;
                if(blockedqR5.size() > 0 && available.R5 > 0)
                {
                	for(int i = 0; i<18; i++){
                                if(blockedqR5.front() == processTable[i].pid)   //if in process table, not terminated
                                {
                                        foundR5 = true;
                                        break;
                                }
                        }
			if(foundR5 ==true){
			        buf0.mtype = blockedqR5.front();
                                if(totalLines < 100000 && verbose == 1){
		                        //printf("Unblocking PID %d by granting request R5 at time %d:%d\n", blockedqR5.front(), *clockSec, *clockNano);
		                        totalLines++;
		                        fprintf(logfile, "Unblocking PID %d by granting request R5 at time %d:%d\n", blockedqR5.front(), *clockSec, *clockNano);
				}
	                        if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
	                                  perror("msgsnd to worker failed\n");
	                                  exit(1);
	                        }
	                        for(int i = 0; i< 18; i++)                                                                                              
				{
	                         	if(processTable[i].pid == blockedqR5.front())
	                                {
	                               		processTable[i].R5 += 1;
	                 			processTable[i].blocked = -1;
			                        break;
	                                }
	                        }
	                        blockedqR5.pop();
	                        available.R5 -= 1;
	                        grantedWaiting++;
                                if(totalLines < 100000){
					totalLines++;
		                        Available(available.R0, available.R1, available.R2, available.R3, available.R4, available.R5, available.R6, available.R7, available.R8, available.R9, logfile);
	                        }
				//printTable(processTable, *clockSec, *clockNano, logfile);
                        }
			else
                        {
                                blockedqR5.pop();
                        }
		}
 		//check blockedq6 to be "unblocked"
                bool foundR6 = false;
                if(blockedqR6.size() > 0 && available.R6 > 0)
                {
			for(int i = 0; i<18; i++){
                                if(blockedqR6.front() == processTable[i].pid)   //if in process table, not terminated
                                {
                                        foundR6 = true;
                                        break;
                                }
                        }
			if(foundR6 == true){
	                        buf0.mtype = blockedqR6.front();
                                if(totalLines < 100000 && verbose == 1){
		                        //printf("Unblocking PID %d by granting request R6 at time %d:%d\n", blockedqR6.front(), *clockSec, *clockNano);
		                        totalLines++;
		                        fprintf(logfile, "Unblocking PID %d by granting request R6 at time %d:%d\n", blockedqR6.front(), *clockSec, *clockNano);
				}
	                        if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
	                                 perror("msgsnd to worker failed\n");
	                                 exit(1);
	                        }
	                        for(int i = 0; i< 18; i++)                                                                                              
				{
	                                 if(processTable[i].pid == blockedqR6.front())
	                                 {
	                                        processTable[i].R6 += 1;
	                			processTable[i].blocked = -1; 
			                        break;
	                                 }
	                        }
	                        blockedqR6.pop();
	                        available.R6 -= 1;
	                        grantedWaiting++;
                                if(totalLines < 100000 ){
					totalLines++;
		                        Available(available.R0, available.R1, available.R2, available.R3, available.R4, available.R5, available.R6, available.R7, available.R8, available.R9, logfile);
				}
	                        //printTable(processTable, *clockSec, *clockNano, logfile);
                	}
                        else
                        {
                                blockedqR6.pop();
                        }
		}
		//check blockedq7 to be "unblocked"
                bool foundR7 = false;
                if(blockedqR7.size() > 0 && available.R7 > 0)
                {
			for(int i = 0; i<18; i++){
                                if(blockedqR7.front() == processTable[i].pid)   //if in process table, not terminated
                                {
                                        foundR7 = true;
                                        break;
                                }
                        }
			if(foundR7 == true){
	                        buf0.mtype = blockedqR7.front();
	                        if(totalLines < 100000 && verbose == 1){
		                        //printf("Unblocking PID %d by granting request R7 at time %d:%d\n", blockedqR7.front(), *clockSec, *clockNano);
		                        totalLines++;
		                        fprintf(logfile, "Unblocking PID %d by granting request R7 at time %d:%d\n", blockedqR7.front(), *clockSec, *clockNano);
				}
	                        if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
	                        	perror("msgsnd to worker failed\n");
	                                exit(1);
	                        }
	                        for(int i = 0; i< 18; i++)                                                                                              
				{
	                                if(processTable[i].pid == blockedqR7.front())
	                                {
	                                        processTable[i].R7 += 1;
	               				processTable[i].blocked = -1;
			                      	break;
	                                }
	                        }
	                        blockedqR7.pop();
	                        available.R7 -= 1;
	                        grantedWaiting++;
                                if(totalLines < 100000){
					totalLines++;
		                        Available(available.R0, available.R1, available.R2, available.R3, available.R4, available.R5, available.R6, available.R7, available.R8, available.R9, logfile);
	                        }
				//printTable(processTable, *clockSec, *clockNano, logfile);
	                }
                        else
                        {
                                blockedqR7.pop();
                        }
		}	
		//check blockedq8 to be "unblocked"
                bool foundR8 = false;
                if(blockedqR8.size() > 0 && available.R8 > 0)
                {
                    	for(int i = 0; i<18; i++){
                                if(blockedqR8.front() == processTable[i].pid)   //if in process table, not terminated
                                {
                                        foundR8 = true;
                                        break;
                                }
                        }
			if(foundR8 == true){
				buf0.mtype = blockedqR8.front();
                                if(totalLines < 100000 && verbose == 1){
		                        //printf("Unblocking PID %d by granting request R8 at time %d:%d\n", blockedqR8.front(), *clockSec, *clockNano);
		                        totalLines++;
		                        fprintf(logfile, "Unblocking PID %d by granting request R8 at time %d:%d\n", blockedqR8.front(), *clockSec, *clockNano);
				}
	                        if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
	                               perror("msgsnd to worker failed\n");
	                               exit(1);
	                        }
	                        for(int i = 0; i< 18; i++)                                                                                              
				{
	                        	if(processTable[i].pid == blockedqR8.front())
	                                {
	                                	processTable[i].R8 += 1;
	                 			processTable[i].blocked = -1;
			                        break;
	                                }
	                        }
	                        blockedqR8.pop();
	                        available.R8 -= 1;
	                        grantedWaiting++;
                                if(totalLines < 100000){
					totalLines++;	
		                        Available(available.R0, available.R1, available.R2, available.R3, available.R4, available.R5, available.R6, available.R7, available.R8, available.R9, logfile);
	     	                }
				//printTable(processTable, *clockSec, *clockNano, logfile);
                	}
                        else
                        {
                                blockedqR8.pop();
                        }
		}
		//check blockedq9 to be "unblocked"
                bool foundR9 = false;
                if(blockedqR9.size() > 0 && available.R9 > 0)
                {
                	for(int i = 0; i<18; i++){
                                if(blockedqR9.front() == processTable[i].pid)   //if in process table, not terminated
                                {
                                        foundR9 = true;
                                        break;
                                }
                        }
			if(foundR9 == true){
				buf0.mtype = blockedqR9.front();
	                        if(totalLines < 100000 && verbose == 1){
		                        //printf("Unblocking PID %d by granting request R9 at time %d:%d\n", blockedqR9.front(), *clockSec, *clockNano);
		                        totalLines++;
		                        fprintf(logfile, "Unblocking PID %d by granting request R9 at time %d:%d\n", blockedqR9.front(), *clockSec, *clockNano);
				}
	                        if (msgsnd(msqid, &buf0, sizeof(msgbuffer)-sizeof(long), 0) == -1){
	                                 perror("msgsnd to worker failed\n");
	                                 exit(1);
	                        }
	                        for(int i = 0; i< 18; i++)   //update it                                                                                           
	                        {
				        if(processTable[i].pid == blockedqR9.front())
	                                {
	                                	processTable[i].R9 += 1;
	                 			processTable[i].blocked = -1;
		                                break;
	                                }
	                        }
	                        blockedqR9.pop();
	                        available.R9 -= 1;
	                        grantedWaiting++;
                                if(totalLines < 100000 && verbose == 1){
					totalLines++;
		                        Available(available.R0, available.R1, available.R2, available.R3, available.R4, available.R5, available.R6, available.R7, available.R8, available.R9, logfile);
	                        }
				//printTable(processTable, *clockSec, *clockNano, logfile);
                	}
                        else
                        {
                                blockedqR9.pop();
                        }
		}

	//if one second has passed
	if(*clockSec > sec)
	{
		sec= *clockSec + 2;	//change speed of deadlock detection
                queue <int>deadlockedq;
		deadlockDetectionRan++;
		deadlockedq = deadlockDetection(processTable, available);
		if(deadlockedq.size() > 0)
		{
			if(totalLines < 99998){
                        	totalLines++;
             	   		fprintf(logfile, "DEADLOCK DETECTION RUNNING AT %d:%d \n", *clockSec, *clockNano);
				totalLines++;
		                fprintf(logfile, "%d Deadlocked Processes: \n", deadlockedq.size());
		                totalLines++;
				showq(deadlockedq, logfile);
                	}
			deadlockFound++;	
			for(int j=0; j<deadlockedq.size(); j++)
			{
				for(int i=0; i<18; i++)
				{
					if(processTable[i].pid == deadlockedq.front())
					{
						//printf("PID %d is killed\n\tResources released: R0:%d, R1:%d, R2:%d, R3:%d, R4:%d, R5:%d, R6:%d, R7:%d, R8:%d, R9:%d\n ", processTable[i].pid, processTable[i].R0, processTable[i].R1,processTable[i].R2,processTable[i].R3, processTable[i].R4, processTable[i].R5, processTable[i].R6, processTable[i].R7, processTable[i].R8, processTable[i].R9 );
			                        totalLines++;
	                                	fprintf(logfile, "PID %d is killed\n\tResources released: R0:%d, R1:%d, R2:%d, R3:%d, R4:%d, R5:%d, R6:%d, R7:%d, R8:%d, R9:%d\n ", processTable[i].pid, processTable[i].R0, processTable[i].R1,processTable[i].R2,processTable[i].R3, processTable[i].R4, processTable[i].R5, processTable[i].R6, processTable[i].R7, processTable[i].R8, processTable[i].R9 );
						available.R0 += processTable[i].R0;
		                                available.R1 += processTable[i].R1;
		                                available.R2 += processTable[i].R2;
		                                available.R3 += processTable[i].R3;
		                                available.R4 += processTable[i].R4;
		                                available.R5 += processTable[i].R5;
		                                available.R6 += processTable[i].R6;
		                                available.R7 += processTable[i].R7;
		                                available.R8 += processTable[i].R8;
		                                available.R9 += processTable[i].R9;	
						kill(processTable[i].pid, SIGKILL);
						removeProcess(processTable, deadlockedq.front());
						deadlockedq.pop();
						deadlockTerm++;
						terminateChild++;
		                	        wait(NULL);
					}
				}
			}
		}
		else
		{
			deadlockNotFound++;
			if(totalLines < 99997 && verbose == 1){
                                totalLines++;
                                fprintf(logfile, "DEADLOCK DETECTION RUNNING AT %d:%d \n", *clockSec, *clockNano);
                                totalLines++;
                                fprintf(logfile, "%d Deadlocked Processes: \n", deadlockedq.size());
				totalLines++;
				fprintf(logfile, "\tNo deadlocks detected\n");
			}
		}
	}


	}
	system("rm msgq.txt");

	//check msg for correct termination
	printf("OSS is now ending.\n");

	printf("STATISTICS\n");
	fprintf(logfile, "STATISTICS\n");
	
	printf("Processes ran: %d, Terminated: %d, Start Time: %d, End Time: %d, Current Time: %d\n", proc, terminateChild, stime,ntime, ctime); 
        fprintf(logfile, "Processes ran: %d, Terminated: %d, Start Time: %d, End Time: %d, Current Time: %d\n", proc, terminateChild, stime,ntime, ctime);

	int totalRequests = grantedImmediately + grantedWaiting;
	printf("Total Requests: %d  -Granted immediately: %d, Granted waiting: %d\n", totalRequests, grantedImmediately, grantedWaiting);
	printf("Terminated:  Successfully: %d    Deadlock Detection: %d\n", successfulTerm, deadlockTerm);
	printf("Deadlock Detection ran for %d times, found: %d  not found: %d\n", deadlockDetectionRan, deadlockFound, deadlockNotFound);
	fprintf(logfile, "Total Requests: %d  -Granted immediately: %d, Granted waiting: %d\n", totalRequests, grantedImmediately, grantedWaiting);
        fprintf(logfile, "Terminated:  Successfully: %d    Deadlock Detection: %d\n", successfulTerm, deadlockTerm);
        fprintf(logfile, "Deadlock Detection ran for %d times, found: %d  not found: %d\n", deadlockDetectionRan, deadlockFound, deadlockNotFound);

	double avgDeadlock = 0;
	avgDeadlock = (double) deadlockTerm / deadlockFound;
	printf("Percentage of processes in a deadlock that had to be terminated on an average: %f\n", avgDeadlock);
	fprintf(logfile, "Percentage of processes in a deadlock that had to be terminated on an average: %f\n", avgDeadlock);

	shmdt( clockSec );    // Detach from the shared memory segment
        shmctl( idSec, IPC_RMID, NULL ); // Free shared memory segment shm_id

	shmdt( clockNano );    // Detach from the shared memory segment
        shmctl( idNano, IPC_RMID, NULL ); // Free shared memory segment shm_id
     
	// get rid of message queue
        if (msgctl(msqid, IPC_RMID, NULL) == -1) {
                perror("msgctl to get rid of queue in parent failed");
                exit(1);
        }

	return EXIT_SUCCESS;
}
